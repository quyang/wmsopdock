package com.xianzaishi.wms.opdock.vo;

public class TokenVO {
	private String token = null;

	private Long agencyID = null;

	private Long govID = null;

	private Long operator = null;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getAgencyID() {
		return agencyID;
	}

	public void setAgencyID(Long agencyID) {
		this.agencyID = agencyID;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Long getGovID() {
		return govID;
	}

	public void setGovID(Long govID) {
		this.govID = govID;
	}
}
