package com.xianzaishi.wmsop.common.filter;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class AccessControAllowOriginFilter extends AbstractInterceptor {

	public String intercept(ActionInvocation invocation) throws Exception {
		ServletActionContext.getResponse().setContentType(
				"text/html;charset=UTF-8");
		ServletActionContext.getResponse().setHeader(
				"Access-Control-Allow-Credentials", "true");
		if (ServletActionContext.getRequest().getHeader("Origin")
				.contains("xianzaishi")) {
			ServletActionContext.getResponse().setHeader(
					"Access-Control-Allow-Origin",
					ServletActionContext.getRequest().getHeader("Origin"));
		}
		return invocation.invoke();
	}

}
