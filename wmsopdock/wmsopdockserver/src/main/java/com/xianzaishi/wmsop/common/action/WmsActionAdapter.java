package com.xianzaishi.wmsop.common.action;

import javax.servlet.http.Cookie;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.purchasecenter.client.organization.OrganizationService;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.opdock.vo.TokenVO;

public abstract class WmsActionAdapter extends ActionAdapter {

	private static final String TOKENKEY = "token";

	@Autowired
	protected BackGroundUserService backgrounduserservice = null;
	@Autowired
	protected OrganizationService organizationService = null;

	protected TokenVO getTokenInfo() {
		BackGroundUserDTO backGroundUserDTO = backgrounduserservice
				.queryUserDTOByToken(getRequestToke()).getModule();
		if (backGroundUserDTO == null) {
			throw new BizException("no user got");
		}

		TokenVO tokenVO = new TokenVO();
		tokenVO.setAgencyID(1l);
		tokenVO.setGovID(1l);
		tokenVO.setOperator(new Long(backGroundUserDTO.getUserId()));
		if (tokenVO == null) {
			throw new BizException("没有获取到d登录信息");
		}
		return tokenVO;
	}

	private String getRequestToke() {
		Cookie[] cookieArray = request.getCookies();
		if (null == cookieArray || cookieArray.length <= 0) {
			throw new BizException("token is empty");
		}
		for (Cookie cookie : cookieArray) {
			if (null != cookie && TOKENKEY.equals(cookie.getName())) {
				return cookie.getValue();
			}
		}
		String flag = ServletActionContext.getRequest().getHeader(TOKENKEY);
		if (flag != null) {
			return flag;
		}
		throw new BizException("token is empty");
	}

	public OrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public BackGroundUserService getBackgrounduserservice() {
		return backgrounduserservice;
	}

	public void setBackgrounduserservice(
			BackGroundUserService backgrounduserservice) {
		this.backgrounduserservice = backgrounduserservice;
	}

}
