package com.xianzaishi.wms.opdock.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IAdjustDomainClient;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class AdjustAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(AdjustAction.class);
	@Autowired
	private IAdjustDomainClient adjustDomainClient = null;

	public String create() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", AdjustVO.class);
			classMap.put("details", AdjustDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo();

			AdjustVO adjustVO = (AdjustVO) simpleRequestVO.getData();
			adjustVO.setAgencyId(tokenVO.getAgencyID());
			adjustVO.setOperate(tokenVO.getOperator());
			adjustVO.setStatu(0);
			flag = adjustDomainClient.createAdjustDomain(adjustVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String queryList() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(AdjustQueryVO.class);
			TokenVO tokenVO = getTokenInfo();

			AdjustQueryVO queryVO = (AdjustQueryVO) simpleRequestVO.getData();
			if (queryVO == null) {
				queryVO = new AdjustQueryVO();
			}

			flag = adjustDomainClient.queryAdjustVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String get() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.getAdjustDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String delete() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.deleteAdjustVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.submit(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String audit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.auditAdjust(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(AdjustDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient
					.modifyAdjustDetailVO((AdjustDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(AdjustDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.batchModifyAdjustDetailVOs(Arrays
					.asList((AdjustDetailVO[]) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = adjustDomainClient.deleteAdjustDetailVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			List<Long> detailIDs = new LinkedList<>();
			for (int i = 0; i < ((String[]) simpleRequestVO.getData()).length; i++) {
				detailIDs.add(Long.parseLong(((String[]) simpleRequestVO
						.getData())[i]));
			}
			flag = adjustDomainClient
					.batchDeleteAdjustDetailVOByAdjustID(detailIDs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(AdjustDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = adjustDomainClient
					.createAdjustDetailVO((AdjustDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(AdjustDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = adjustDomainClient.batchCreateAdjustDetailVO(Arrays
					.asList((AdjustDetailVO[]) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getAdjustAllDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			flag = adjustDomainClient.getAdjustDetailVOListByAdjustID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IAdjustDomainClient getAdjustDomainClient() {
		return adjustDomainClient;
	}

	public void setAdjustDomainClient(IAdjustDomainClient adjustDomainClient) {
		this.adjustDomainClient = adjustDomainClient;
	}

}
