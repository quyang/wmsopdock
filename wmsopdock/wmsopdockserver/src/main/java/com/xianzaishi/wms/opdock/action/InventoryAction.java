package com.xianzaishi.wms.opdock.action;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class InventoryAction extends WmsActionAdapter {
	public static final Logger logger = Logger.getLogger(InventoryAction.class);

	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;

	public String getPositionBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo();
			flag = inventoryDomainClient.getInventoryBySKUID(
					tokenVO.getAgencyID(),
					Long.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getInventoryByPositionAndSku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(InventoryQueryVO.class);
			TokenVO tokenVO = getTokenInfo();
			InventoryQueryVO inventoryQueryVO = (InventoryQueryVO) simpleRequestVO
					.getData();
			if (inventoryQueryVO == null
					|| inventoryQueryVO.getPositionId() == null
					|| inventoryQueryVO.getSkuId() == null) {
				throw new BizException("position or sku error!");
			}
			flag = inventoryDomainClient.getInventoryByPositionAndSku(
					inventoryQueryVO.getPositionId(),
					inventoryQueryVO.getSkuId());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String recommendPositionBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo();
			Long skuID = Long.parseLong((String) simpleRequestVO.getData());
			SimpleResultVO<List<PositionDetailVO>> positionDetailVOs = inventoryConfigDomainClient
					.getPositionDetailBySkuID(tokenVO.getAgencyID(), skuID);
			if (positionDetailVOs.getTarget() != null
					&& positionDetailVOs.getTarget().size() > 0) {
				for (PositionDetailVO positionDetailVO : positionDetailVOs
						.getTarget()) {
					positionDetailVO
							.setPisitionVO(positionDomainClient
									.getPositionVOByID(
											positionDetailVO.getPositionId())
									.getTarget());
				}
			}
			flag = positionDetailVOs;
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

}
