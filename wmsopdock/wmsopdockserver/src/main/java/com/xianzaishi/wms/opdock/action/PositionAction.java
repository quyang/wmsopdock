package com.xianzaishi.wms.opdock.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class PositionAction extends WmsActionAdapter {
	public static final Logger logger = Logger.getLogger(PositionAction.class);

	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public String add() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PositionVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = positionDomainClient
					.addPosition((PositionVO) simpleRequestVO.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

}
