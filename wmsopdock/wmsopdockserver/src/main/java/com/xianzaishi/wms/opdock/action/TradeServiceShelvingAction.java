package com.xianzaishi.wms.opdock.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.job.domain.client.itf.IJobDomainClient;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.ITradeServiceShelvingDomainClient;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;
import com.xianzaishi.wms.trade.callback.client.itf.ITrackCallbackClient;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class TradeServiceShelvingAction extends WmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(TradeServiceShelvingAction.class);
	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;
	@Autowired
	private ITradeServiceShelvingDomainClient tradeServiceShelvingDomainClient = null;
	@Autowired
	private ITrackCallbackClient trackCallbackClient = null;
	@Autowired
	private IJobDomainClient jobDomainClient = null;

	public String create() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", TradeServiceShelvingVO.class);
			classMap.put("details", TradeServiceShelvingDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo();

			TradeServiceShelvingVO tradeServiceShelvingVO = (TradeServiceShelvingVO) simpleRequestVO
					.getData();
			tradeServiceShelvingVO.setAgencyId(tokenVO.getAgencyID());
			tradeServiceShelvingVO.setOperate(tokenVO.getOperator());
			tradeServiceShelvingVO.setStatu(0);
			flag = tradeServiceShelvingDomainClient
					.createTradeServiceShelvingDomain(tradeServiceShelvingVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String queryList() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingQueryVO.class);
			TokenVO tokenVO = getTokenInfo();

			TradeServiceShelvingQueryVO queryVO = (TradeServiceShelvingQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new TradeServiceShelvingQueryVO();
			}

			flag = tradeServiceShelvingDomainClient
					.queryTradeServiceShelvingVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String get() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient
					.getTradeServiceShelvingDomainByID(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String delete() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient
					.deleteTradeServiceShelvingVO(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient.submit(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String audit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient.audit(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient
					.modifyTradeServiceShelvingDetailVO((TradeServiceShelvingDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient
					.batchModifyTradeServiceShelvingDetailVOs(Arrays
							.asList((TradeServiceShelvingDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = tradeServiceShelvingDomainClient
					.deleteTradeServiceShelvingDetailVO(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			List<Long> detailIDs = new LinkedList<>();
			for (int i = 0; i < ((String[]) simpleRequestVO.getData()).length; i++) {
				detailIDs.add(Long.parseLong(((String[]) simpleRequestVO
						.getData())[i]));
			}
			flag = tradeServiceShelvingDomainClient
					.batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(detailIDs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = tradeServiceShelvingDomainClient
					.createTradeServiceShelvingDetailVO((TradeServiceShelvingDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = tradeServiceShelvingDomainClient
					.batchCreateTradeServiceShelvingDetailVO(Arrays
							.asList((TradeServiceShelvingDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getTradeServiceShelvingAllDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			flag = tradeServiceShelvingDomainClient
					.getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String onOrderCreate() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			flag = trackCallbackClient.onOrderCreate(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchOnCreatOrder() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			JobQueryVO jobQueryVO = new JobQueryVO();
			jobQueryVO.setType(4);
			jobQueryVO.setStatu(1);
			List<JobVO> jobs = jobDomainClient.queryJobVOList(jobQueryVO)
					.getTarget();

			for (JobVO jobVO : jobs) {
				JSONObject jsonObject = JSONObject.fromObject(jobVO
						.getParameter());
				flag = trackCallbackClient.onOrderCreate(new Long(
						(Integer) jsonObject.get("id")));
				logger.error("order : " + jsonObject.get("id")
						+ " , message : " + flag.getMessage());
				jobDomainClient.processed(jobVO.getId());
			}

		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

	public ITradeServiceShelvingDomainClient getTradeServiceShelvingDomainClient() {
		return tradeServiceShelvingDomainClient;
	}

	public void setTradeServiceShelvingDomainClient(
			ITradeServiceShelvingDomainClient tradeServiceShelvingDomainClient) {
		this.tradeServiceShelvingDomainClient = tradeServiceShelvingDomainClient;
	}

	public ITrackCallbackClient getTrackCallbackClient() {
		return trackCallbackClient;
	}

	public void setTrackCallbackClient(ITrackCallbackClient trackCallbackClient) {
		this.trackCallbackClient = trackCallbackClient;
	}

	public IJobDomainClient getJobDomainClient() {
		return jobDomainClient;
	}

	public void setJobDomainClient(IJobDomainClient jobDomainClient) {
		this.jobDomainClient = jobDomainClient;
	}

}
