package com.xianzaishi.wms.opdock.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IStorageAreaDomainClient;
import com.xianzaishi.wms.hive.vo.StoryAreaVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class StorageAreaAction extends WmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(StorageAreaAction.class);

	@Autowired
	private IStorageAreaDomainClient storageAreaDomainClient = null;

	public String add() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(StoryAreaVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = storageAreaDomainClient
					.addStorageArea((StoryAreaVO) simpleRequestVO.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IStorageAreaDomainClient getStorageAreaDomainClient() {
		return storageAreaDomainClient;
	}

	public void setStorageAreaDomainClient(
			IStorageAreaDomainClient storageAreaDomainClient) {
		this.storageAreaDomainClient = storageAreaDomainClient;
	}

}
