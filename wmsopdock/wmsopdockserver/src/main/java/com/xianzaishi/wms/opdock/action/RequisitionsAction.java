package com.xianzaishi.wms.opdock.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IRequisitionsDomainClient;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class RequisitionsAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(RequisitionsAction.class);
	@Autowired
	private IRequisitionsDomainClient requisitionsDomainClient = null;

	public String create() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", RequisitionsVO.class);
			classMap.put("details", RequisitionsDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo();

			RequisitionsVO requisitionsVO = (RequisitionsVO) simpleRequestVO
					.getData();
			requisitionsVO.setAgencyId(tokenVO.getAgencyID());
			requisitionsVO.setOperate(tokenVO.getOperator());
			requisitionsVO.setStatu(Short.parseShort("0"));
			flag = requisitionsDomainClient
					.createRequisitionsDomain(requisitionsVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String queryList() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsQueryVO.class);
			TokenVO tokenVO = getTokenInfo();

			RequisitionsQueryVO queryVO = (RequisitionsQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new RequisitionsQueryVO();
			}

			flag = requisitionsDomainClient.queryRequisitionsVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String get() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient.getRequisitionsDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String delete() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient.deleteRequisitionsVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient.submit(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String audit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient.audit(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient
					.modifyRequisitionsDetailVO((RequisitionsDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient
					.batchModifyRequisitionsDetailVOs(Arrays
							.asList((RequisitionsDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = requisitionsDomainClient.deleteRequisitionsDetailVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			List<Long> detailIDs = new LinkedList<>();
			for (int i = 0; i < ((String[]) simpleRequestVO.getData()).length; i++) {
				detailIDs.add(Long.parseLong(((String[]) simpleRequestVO
						.getData())[i]));
			}
			flag = requisitionsDomainClient
					.batchDeleteRequisitionsDetailVOByRequisitionsID(detailIDs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = requisitionsDomainClient
					.createRequisitionsDetailVO((RequisitionsDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = requisitionsDomainClient
					.batchCreateRequisitionsDetailVO(Arrays
							.asList((RequisitionsDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getRequisitionsAllDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			flag = requisitionsDomainClient
					.getRequisitionsDetailVOListByRequisitionsID(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IRequisitionsDomainClient getRequisitionsDomainClient() {
		return requisitionsDomainClient;
	}

	public void setRequisitionsDomainClient(
			IRequisitionsDomainClient requisitionsDomainClient) {
		this.requisitionsDomainClient = requisitionsDomainClient;
	}
}
