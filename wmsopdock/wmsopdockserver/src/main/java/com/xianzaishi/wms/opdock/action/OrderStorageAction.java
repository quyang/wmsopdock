package com.xianzaishi.wms.opdock.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IBookingDeliveryDomainClient;
import com.xianzaishi.wms.track.domain.client.itf.IInspectionDomainClient;
import com.xianzaishi.wms.track.domain.client.itf.IOrderStorageDomainClient;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class OrderStorageAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(OrderStorageAction.class);
	@Autowired
	private IOrderStorageDomainClient orderStorageDomainClient = null;
	@Autowired
	private IBookingDeliveryDomainClient bookingDeliveryDomainClient = null;
	@Autowired
	private IInspectionDomainClient inspectionDomainClient = null;

	public IOrderStorageDomainClient getOrderStorageDomainClient() {
		return orderStorageDomainClient;
	}

	public String create() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", OrderStorageVO.class);
			classMap.put("details", OrderStorageDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo();

			OrderStorageVO orderStorageVO = (OrderStorageVO) simpleRequestVO
					.getData();
			orderStorageVO.setAgencyId(tokenVO.getAgencyID());
			orderStorageVO.setOperate(tokenVO.getOperator());
			orderStorageVO.setStatu(0);
			flag = orderStorageDomainClient
					.createOrderStorageDomain(orderStorageVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String queryList() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(OrderStorageQueryVO.class);
			TokenVO tokenVO = getTokenInfo();

			OrderStorageQueryVO queryVO = (OrderStorageQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new OrderStorageQueryVO();
			}

			flag = orderStorageDomainClient.queryOrderStorageVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String get() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient.getOrderStorageDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String delete() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient.deleteOrderStorageVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient.submit(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String audit() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient.audit(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(OrderStorageDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient
					.modifyOrderStorageDetailVO((OrderStorageDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String updateDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(OrderStorageDetailVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient
					.batchModifyOrderStorageDetailVOs(Arrays
							.asList((OrderStorageDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = orderStorageDomainClient.deleteOrderStorageDetailVO(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deleteDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			List<Long> detailIDs = new LinkedList<>();
			for (int i = 0; i < ((String[]) simpleRequestVO.getData()).length; i++) {
				detailIDs.add(Long.parseLong(((String[]) simpleRequestVO
						.getData())[i]));
			}
			flag = orderStorageDomainClient
					.batchDeleteOrderStorageDetailVOByOrderStorageID(detailIDs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(OrderStorageDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = orderStorageDomainClient
					.createOrderStorageDetailVO((OrderStorageDetailVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(OrderStorageDetailVO.class);
			TokenVO tokenVO = getTokenInfo();

			flag = orderStorageDomainClient
					.batchCreateOrderStorageDetailVO(Arrays
							.asList((OrderStorageDetailVO[]) simpleRequestVO
									.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getOrderStorageAllDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			flag = orderStorageDomainClient
					.getOrderStorageDetailVOListByOrderStorageID(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public void setOrderStorageDomainClient(
			IOrderStorageDomainClient orderStorageDomainClient) {
		this.orderStorageDomainClient = orderStorageDomainClient;
	}

	public IBookingDeliveryDomainClient getBookingDeliveryDomainClient() {
		return bookingDeliveryDomainClient;
	}

	public void setBookingDeliveryDomainClient(
			IBookingDeliveryDomainClient bookingDeliveryDomainClient) {
		this.bookingDeliveryDomainClient = bookingDeliveryDomainClient;
	}

	public IInspectionDomainClient getInspectionDomainClient() {
		return inspectionDomainClient;
	}

	public void setInspectionDomainClient(
			IInspectionDomainClient inspectionDomainClient) {
		this.inspectionDomainClient = inspectionDomainClient;
	}

}
