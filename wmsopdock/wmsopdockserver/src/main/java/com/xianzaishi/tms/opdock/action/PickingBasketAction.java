package com.xianzaishi.tms.opdock.action;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IPickingBasketDomainClient;
import com.xianzaishi.wms.tmscore.vo.PickingBasketVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;

public class PickingBasketAction extends WmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(PickingBasketAction.class);

	@Autowired
	private IPickingBasketDomainClient pickingBasketDomainClient = null;

	public String getPickingBasketByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			String barcode = (String) simpleRequestVO.getData();
			flag = pickingBasketDomainClient
					.getPickingBasketVOByBarcode(barcode);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getPickingBasketVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			Long id = Long.parseLong((String) simpleRequestVO.getData());
			flag = pickingBasketDomainClient.getPickingBasketVOByID(id);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetPickingBasketVOByBarcode() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> barcodes = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<String> sbarcodes = new LinkedList<String>();
			for (Object object : barcodes) {
				sbarcodes.add((String) object);
			}
			flag = pickingBasketDomainClient
					.batchGetPickingBasketVOByBarcode(sbarcodes);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String batchGetPickingBasketVOByID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			List<Object> ids = Arrays.asList((Object[]) simpleRequestVO
					.getData());
			List<Long> sids = new LinkedList<Long>();
			for (Object object : ids) {
				sids.add(Long.parseLong((String) object));
			}
			flag = pickingBasketDomainClient.batchGetPickingBasketVOByID(sids);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String addPickingBasket() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PickingBasketVO.class);
			flag = pickingBasketDomainClient
					.addPickingBasketVO((PickingBasketVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IPickingBasketDomainClient getPickingBasketDomainClient() {
		return pickingBasketDomainClient;
	}

	public void setPickingBasketDomainClient(
			IPickingBasketDomainClient pickingBasketDomainClient) {
		this.pickingBasketDomainClient = pickingBasketDomainClient;
	}

}
