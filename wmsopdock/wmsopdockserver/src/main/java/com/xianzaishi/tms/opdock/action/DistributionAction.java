package com.xianzaishi.tms.opdock.action;

import com.opensymphony.xwork2.ActionContext;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.utils.enums.OrderStatus;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.opdock.vo.TokenVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionDetailVO;
import com.xianzaishi.wms.tmscore.vo.DistributionQueryVO;
import com.xianzaishi.wms.tmscore.vo.DistributionVO;
import com.xianzaishi.wmsop.common.action.WmsActionAdapter;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class DistributionAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(DistributionAction.class);
	@Autowired
	private IDistributionDomainClient distributionDomainClient = null;

	@Autowired
	private OrderService orderService = null;

	public String queryByPage() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(DistributionQueryVO.class);
			TokenVO tokenVO = getTokenInfo();

			DistributionQueryVO distributionQueryVO = null;
			if (simpleRequestVO == null
					|| (distributionQueryVO = (DistributionQueryVO) simpleRequestVO
							.getData()) == null) {
				distributionQueryVO = new DistributionQueryVO();
				distributionQueryVO.setBizStatuNE(1);
				distributionQueryVO.setStatu(new Short("2"));
				distributionQueryVO.setAgencyId(tokenVO.getAgencyID());
			}

			flag = distributionDomainClient
					.queryDistributionVOListByPage(distributionQueryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String get() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient.getDistributionDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String close() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(DistributionVO.class);
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient
					.closeDistribution((DistributionVO) simpleRequestVO
							.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String arrived() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", DistributionVO.class);
			classMap.put("details", DistributionDetailVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<DistributionVO> simpleRequestVO = (SimpleRequestVO<DistributionVO>) JSONObject
					.toBean(tmp, SimpleRequestVO.class, classMap);
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient
					.arrived((DistributionVO) simpleRequestVO.getData());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String pickStart() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient.pickStart(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String picked(Long id) {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient.picked(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String packStart(Long id) {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient.packStart(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String packed(Long id) {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();
			flag = distributionDomainClient.packed(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String deilverStart() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo();

			String peisongId = (String) simpleRequestVO.getData();

			SimpleResultVO<DistributionVO> distributionVOResult = distributionDomainClient.getDistributionDomainByID(Long.parseLong(peisongId));
			if (null != distributionVOResult && distributionVOResult.getTarget() != null) {
				DistributionVO distributionVO = distributionVOResult.getTarget();
				if (orderService == null) {
					logger.error("订单服务为空");
				}
				if (distributionVO.getOrderId() != null && distributionVO.getOrderId() > 0) {
					logger.error("订单id=" + distributionVO.getOrderId() + ",orderService=null?:" + (null == orderService));
					com.xianzaishi.trade.client.Result<Boolean> result = orderService.updateOrderStatus(distributionVO.getOrderId(), OrderStatus.DISPATCHING.getValue());
					if(null == result || null == result.getModel() || !result.getModel()){
						logger.error("修改订单失败，原因是：" + JackSonUtil.getJson(result));
						pushResult(result);
						return JackSonUtil.getJson(result);
					}
					boolean a = result.getModel();
					logger.error("修改订单状态=" + a);
				} else {
					logger.error("修改订单状态失败，获取订单id为空或者为0");
				}
			} else {
				logger.error("修改订单状态失败，查询配送对象信息错误");
			}
			flag = distributionDomainClient.deilverStart(Long
					.parseLong((String) simpleRequestVO.getData()));

		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IDistributionDomainClient getDistributionDomainClient() {
		return distributionDomainClient;
	}

	public void setDistributionDomainClient(
			IDistributionDomainClient distributionDomainClient) {
		this.distributionDomainClient = distributionDomainClient;
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
}
